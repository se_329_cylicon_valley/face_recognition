Current functionality:
-When run, starts a video feed from the webcam
-If user presses spacebar, takes a picture and displays it
--If user presses ESC or spacebar, return to video feed
-If user presses ESC, exit application

Compiling the project from command line:

(FOR MAC)

>>cd <path/to/application/folder>
>>/Applications/CMake.app/Contents/bin/cmake .
>>make
